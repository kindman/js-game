'use strict';

class Vector {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    plus(vectorObject) {
        if (!(vectorObject instanceof Vector)) {
            throw new Error('Можно прибавлять к вектору только вектор типа Vector');
        }
        return new Vector(this.x + vectorObject.x, this.y + vectorObject.y);
    }

    times(multiplier = 1) {
        return new Vector(this.x * multiplier, this.y * multiplier);
    }
}

class Actor {
    constructor(pos = new Vector(0, 0), size = new Vector(1, 1), speed = new Vector(0, 0)) {
        if (!(pos instanceof Vector && size instanceof Vector && speed instanceof Vector)) {
            throw new Error('Actor может принимать только объекты типа Vector')
        }
        this.pos = pos;
        this.size = size;
        this.speed = speed;
    }
    act() {}
    get type() {
        return 'actor';
    }
    get left() {
        return this.pos.x;
    }
    get top() {
        return this.pos.y;
    }
    get right() {
        return this.pos.x + this.size.x;
    }
    get bottom() {
        return this.pos.y + this.size.y;
    }
    isIntersect(actor) {
        if (!(actor instanceof Actor) || actor === undefined) {
            throw new Error('Метод isIntersect может принимать только объекты типа Actor');
        }

        if (actor === this) {
            return false;
        }

        return !(this.left >= actor.right || this.right <= actor.left || this.top >= actor.bottom || this.bottom <= actor.top);
    }
}

class Level {
    constructor(grid = [], actors = []) {

        this.grid = grid;
        this.actors = actors;
        this.player = this.actors.find(element => element.type === 'player');

        if (this.grid.length === 0) {
            this.height = 0;
            this.width = 0;
        } else {
            this.height = this.grid.length;
            this.width = Math.max(...this.grid.map(arr => arr.length));
        }
        this.status = null;
        this.finishDelay = 1
    }
    isFinished() {
        return this.status !== null && this.finishDelay < 0;
    }

    actorAt(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error('Ошибка в методе actorAt. Метод принимает объект Actor')
        }

        return this.actors.find(element => element.isIntersect(actor));
    }

    obstacleAt(moveObj, sizeObj) {
        if (!(moveObj instanceof Vector && sizeObj instanceof Vector)) {
            throw new Error('Метод obstacleAt может принимать только объекты типа Vector')
        }

        const left = Math.floor(moveObj.x);
        const right = Math.ceil(moveObj.x + sizeObj.x);
        const top = Math.floor(moveObj.y);
        const bottom = Math.ceil(moveObj.y + sizeObj.y);

        if (bottom > this.height) {
            return 'lava';
        }
        if (left < 0 || right > this.width || top < 0) {
            return 'wall';
        }

        for (let y = top; y < bottom; y++) {
            for (let x = left; x < right; x++) {
                const barrier = this.grid[y][x];
                if (barrier) {
                    return barrier;
                }
            }
        }

    }

    removeActor(actorObj) {
        this.actors = this.actors.filter(element => actorObj !== element);
    }

    noMoreActors(actor) {
        return !this.actors.some(element => element.type === actor);
    }

    playerTouched(actorType, actor) {

        if (this.status !== null) {
            return;
        }

        if (actorType === 'lava' || actorType === 'fireball') {
            this.status = 'lost';
        }

        if (actorType === 'coin' && actor.type === 'coin') {
            this.removeActor(actor);
            if (this.noMoreActors(actorType)) {
                this.status = 'won';
            }
        }
    }
}

class LevelParser {
    constructor(dict = {}) {
        this.dict = {
            ...dict
        }
    }

    actorFromSymbol(symbol) {
        return this.dict[symbol];
    }

    obstacleFromSymbol(symbol) {
        if (symbol === 'x') {
            return 'wall';
        }
        if (symbol === '!') {
            return 'lava';
        }
        return undefined;
    }

    createGrid(gridStrings) {
        return gridStrings.map(element => element.split('').map(el => this.obstacleFromSymbol(el)));
    }

    createActors(gridStrings) {
        const arr = [];
        for (let y = 0; y < gridStrings.length; y++) {
            for (let x = 0; x < gridStrings[y].length; x++) {
                const actorSymbol = this.actorFromSymbol(gridStrings[y][x]);
                if (typeof actorSymbol === 'function') {
                    const vector = new Vector(x, y);
                    const movObj = new actorSymbol(vector);
                    if (movObj instanceof Actor) {
                        arr.push(movObj);
                    }
                }
            }
        }
        return arr;
    }
    parse(gridStrings) {
        return new Level(this.createGrid(gridStrings), this.createActors(gridStrings));
    }
}


class Fireball extends Actor {
    constructor(pos = new Vector(0, 0), speed = new Vector(0, 0)) {
        const size = new Vector(1, 1);
        super(pos, size, speed);
    }
    get type() {
        return 'fireball';
    }
    getNextPosition(time = 1) {
        return this.speed.times(time).plus(this.pos);
    }
    handleObstacle() {
        this.speed = this.speed.times(-1);
    }
    act(time, level) {
        const obstacle = level.obstacleAt(this.getNextPosition(time), this.size);
        if (obstacle) {
            this.handleObstacle();
        } else {
            this.pos = this.getNextPosition(time);
        }
    }
}

class HorizontalFireball extends Fireball {
    constructor(pos = new Vector(0, 0)) {
        const speed = new Vector(2, 0);
        super(pos, speed);
    }
}


class VerticalFireball extends Fireball {
    constructor(pos = new Vector(0, 0)) {
        const speed = new Vector(0, 2);
        super(pos, speed);
    }
}

class FireRain extends Fireball {
    constructor(pos = new Vector(0, 0)) {
        super(pos, new Vector(0, 3));
        this.startPosition = this.pos;
    }
    handleObstacle() {
        this.pos = this.startPosition;
    }
}

class Coin extends Actor {
    constructor(pos = new Vector(0, 0)) {
        super(new Vector(pos.x + 0.2, pos.y + 0.1), new Vector(0.6, 0.6));

        this.currentPosition = this.pos;
        this.springSpeed = 8;
        this.springDist = 0.07;
        this.spring = Math.round((Math.random() * 2 * Math.PI) * 100) / 100;
    }
    get type() {
        return 'coin';
    }

    updateSpring(time = 1) {
        this.spring += this.springSpeed * time;
    }

    getSpringVector() {
        const y = Math.sin(this.spring) * this.springDist;
        return new Vector(0, y);
    }

    getNextPosition(time) {
        this.updateSpring(time);
        this.pos = this.currentPosition.plus(this.getSpringVector());
        return this.pos;
    }

    act(time) {
        this.pos = this.getNextPosition(time);
    }
}

class Player extends Actor {
    constructor(pos = new Vector(0, 0)) {
        super(pos.plus(new Vector(0, -0.5)), new Vector(0.8, 1.5));
    }
    get type() {
        return 'player';
    }
}


loadLevels()
    .then(level => {
        const schemas = JSON.parse(level);
        const actorDict = {
            '@': Player,
            'v': FireRain,
            'o': Coin,
            '=': HorizontalFireball,
            '|': VerticalFireball
        }
        const parser = new LevelParser(actorDict);

        runGame(schemas, parser, DOMDisplay).then(() => alert('Вы выиграли!'));
    })
    .catch(function(err) {
        throw new Error(err);
    });